#ifndef FBBENCH_MCTEST_H
#define FBBENCH_MCTEST_H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

/* binary_op.h */
void compose_binary_noop(char *buf);
void compose_binary_set(char *request, char *key, char *value, int *body_len);
int compose_binary_get(char *request, char *key, int cmd, int ip_rank, int port);
size_t binary_mget_req(char *, char **, int);
int binary_get_req(char *, char *, int, int);
size_t compose_binary_mget_req_h(char *request, int batch_sz, size_t bodylen);
uint16_t get_ip_from_resp_opaque(char *h);
uint16_t get_port_from_resp_opaque(char *h);
uint32_t get_sfd_from_opaque(char *h);
void fill_ip_port_to_req_h(char *h, uint16_t ip, uint16_t port);
void fill_sfd_to_req_h(char *h, uint32_t sfd);

/* sendrecv.h */
void binary_set_or_noop(char *key, char *value, bool is_noop);
void binary_get(char **keys, int batch_sz, bool mget_mode);
size_t send_request(int sfd, char *request, size_t len);
size_t recv_request(int sfd, char *recv_buf, size_t len);
void create_udp_connection(char *, int);

uint32_t u_global_req_id;

#endif //FBBENCH_MCTEST_H
