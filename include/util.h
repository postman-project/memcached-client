#ifndef MCPROXY_UTIL_H
#define MCPROXY_UTIL_H

#include <stdint.h>
#include "general.h"

/* socket.h */
int waitread(int, int);
int waitwrite(int, int);
int set_nodelay(int, int);
int set_nonblock(int fd);
int clientsock(char *, int);
int serversock(int port);
void ascii_set_request(int, char *, char *, int);
void ascii_get_request(int, char **, int);
void ascii_parse_get_response(int);
void ascii_parse_set_response(int);

/* conn.h */
int unused_fd(int);
int release_fd(int);
void ensure_readable(int, int);
void init_server_fds(int, int *);
//int *alloc_fds(int);
//void release_fds(int *);
//void init_proxy_conn(int, int *, int);
//void release_proxy_conn(int);
void init_proxy_conn_server_list(proxy_conn_server_list *pcs, server_config *config);
void release_proxy_conn_server_list(proxy_conn_server_list *pcs, int num_conn);
void init_proxy_conn_client_entry(proxy_conn_client_list *pcc, int fd, int port, char *server);

/* parse_ini_file.h */
int parse_ini_file(char *);
int find_port_by_server(char *);

/* udp.h */
void to_udp_header(char*, uint32_t);
void parse_udp_header(char *);

#endif //MCPROXY_UTIL_H
