#!/bin/bash
## ref: https://docs.oracle.com/cd/E17952_01/refman-5.6-en/ha-memcached-using.html


mcserver=/users/shir/myproject/routerbatching/memcached/svn-mc-install/bin/memcached
#mcserver=/users/shir/dev-install/memcached-dbg/bin/memcached
#mcserver=/users/shir/dev-install/memcached/bin/memcached

#host=localhost
#host=node730-3
host=node220-20
tcpport=11222

# 0 to disable udpport
udpport=11777

port_str="-p $tcpport"
#port_str="-U $udpport -p 0"

pidfile=/tmp/memc.pid
logdir=/users/shir/myproject/routerbatching/memcached/perftest/output/log

numcores=8 ##16
bprotocol=auto #ascii #binary
#maxmem=$((1024*16)) ##730-x
maxmem=$((1024*10))  ##220-x

verboselevel="-vvv"

ops=$1
if [[ $ops = "start" ]]
then
    touch $pidfile
    #memcached -m 8192 -l $host -p $portnum -P $pidfile -v -d
    $mcserver -l $host ${port_str} -m $maxmem -t $numcores -B $bprotocol -P $pidfile $verboselevel -d &> $logdir/server-log-$host
    sleep 1
    # print process (used for kill later)
    #pgrep memcached
    echo -e "cat $pidfile => `cat $pidfile` \n"

elif [[ $ops = "stop" ]]
then
    echo -e "before stop pgrep memcached => `pgrep memcached`"
    sudo kill -9 `cat $pidfile`
    echo -e "after stop pgrep memcached => `pgrep memcached` \n"

else
  echo -e "please input either 'start' or 'stop'\n"
  exit 1
fi
