#include <stdio.h>
//#include "mc.h"
#include "util.h"
#include <netinet/in.h>

void to_udp_header(char* buf, uint32_t req_id) {
    *buf++ = req_id >> 0x8;
    *buf++ = req_id & 0xff;
    *buf++ = 0;
    *buf++ = 0;
    *buf++ = 0;
    *buf++ = 1;

    *buf++ = 0;
    *buf++ = 0;
}

void parse_udp_header(char *recv_buf) {
    uint16_t req_id = htons(((uint16_t *)recv_buf)[0]);
    uint16_t seq_no = htons(((uint16_t *)recv_buf)[1]);
    uint16_t n_parts = htons(((uint16_t *)recv_buf)[2]);
    uint16_t reserved = htons(((uint16_t *)recv_buf)[3]);
    fprintf(stderr, "reply udp_header req_id=%u seq_id=%u n_parts=%u reserved=%u\n",
            req_id, seq_no, n_parts, reserved);
}

