#!/bin/bash
topdir=/home/guojian/mc_test/mcproxy
exe=mcclient

server_host=10.0.11.24
tport=11211
uport=11777

mc_host=node220-18
mcport=11888

myhost=`hostname`
key_prefix=${myhost:${#myhost} - 2}

# # through mcrouter
# host=${mc_host}
# port=$mcport

# direct
host=${server_host}
port=$tport

num_keys=200
k_len=16
v_len=16
outfile=mget-b4-client-udp
use_ascii=0
use_udp=0
config_file=$topdir/mcconfig.ini

#tcpdump_file=dump-19-18
#touch ${tcpdump_file}
#sudo tcpdump host 10.0.0.29 and 10.0.0.28 -vv -X &> ${tcpdump_file}

#./mcbench -c 1 -g 100 -i 5 -k 16 -r 16 -u 0 $host $port #&> $outfile
# echo -e "
$topdir/$exe -c 1 -k ${num_keys} -x ${key_prefix} -t 1 -a ${use_ascii} -u ${use_udp} \
    -l ${k_len} -v ${v_len} -f ${config_file} -g 0  $host $port
# "
