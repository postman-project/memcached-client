#!/bin/bash
if [ $# -ne 7 ]; then
   echo -e "Usage: ./client-run.sh [#process] [#clients] [rec_time] [total_time] [upoutdir] [pure_get]"
   echo -e "#process: number of ctest processes"
   echo -e "#clients: number of clients to launch the processes"
   echo -e "rec_time/total_time: record time and total execution time interval"
   echo -e "upoutdir: path to result"
   echo -e "pure_get"
   echo -e "ip"
   exit 1
fi

procs=$1
nclients=$2
rectm=$3
maxtm=$4
upoutdir=$5   # consistent with run-kill.sh
method=$6

topdir=/proj/postman-PG0/mc_test/mcproxy
exe=mcclient
#exe=unit

server_host=$7
tport=11211
uport=11777

proxy_host=node220-19
proxy_port=22222

# # through mcproxy
# host=${proxy_host}
# port=${proxy_port}

# direct
host=${server_host}
port=$tport
num_keys=16
k_len=16  # keylen
v_len=16
outfile=mget-b4-client-udp
use_ascii=0
use_udp=0
config_file=$topdir/mcconfig.ini
rec_interval=5
rec_round=4
warmup_time=10
num_threads=4

use_fix_keys=1
pure_get=$6
batch_size=4
header_op=0

#tcpdump_file=dump-19-18
#touch ${tcpdump_file}
#sudo tcpdump host 10.0.0.29 and 10.0.0.28 -vv -X &> ${tcpdump_file}

#./mcbench -c 1 -g 100 -i 5 -k 16 -r 16 -u 0 $host $port #&> $outfile

key_prefix=
OUTFILE=
runtest() {
    OUTFILE=$upoutdir/${outfile}
    touch $OUTFILE
    key_prefix=4
    #echo -e "
  	$topdir/$exe -c 1000  -k ${num_keys} -x ${key_prefix} -a ${use_ascii} -u ${use_udp} \
        -i ${rec_interval} -l ${k_len} -v ${v_len} -f ${config_file}  -n ${rec_round}  \
        -t ${num_threads} -w ${warmup_time} -m ${use_fix_keys}  -g ${pure_get} -h ${header_op} \
        $host $port  | tee -a $OUTFILE
    #"
}


## main routine ##
myhost=`hostname`
prefix=${myhost:${#myhost} - 2}
for incproc in $(eval echo {1..$(($procs/$nclients))})
do
  runtest c${prefix}i${incproc} &
done

wait
echo -e "client-run.sh done with [$procs] test\n"
